
#include "image_worker.h"

struct image image_allocate(uint64_t width, uint64_t height) {
    struct image img = {0};
    img.width = width;
    img.height = height;
    size_t pixel_size = sizeof(struct pixel);
    img.data = malloc(pixel_size * width * height);
    return img;
}

void image_free(struct image* image) {
    image->width = 0;
    image->height = 0;
    free(image->data);
}
