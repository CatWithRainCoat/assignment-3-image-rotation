#ifndef IMAGE_TRANSFORMER_IMAGE_WORKER_H
#define IMAGE_TRANSFORMER_IMAGE_WORKER_H

#include <inttypes.h>
#include <malloc.h>

struct pixel {
    uint8_t g, b, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_free(struct image* image);
struct image image_allocate(uint64_t width, uint64_t height);


#endif //IMAGE_TRANSFORMER_IMAGE_WORKER_H
