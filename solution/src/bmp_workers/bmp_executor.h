#ifndef IMAGE_TRANSFORMER_BMP_EXECUTOR_H
#define IMAGE_TRANSFORMER_BMP_EXECUTOR_H

#include "../file_workers/file_worker.h"
#include "../utils/utils.h"
#include "bmp_reader/bmp_reader.h"
#include "bmp_transformer/bmp_rotator.h"
#include "bmp_writer/bmp_writer.h"

#include <stdbool.h>

bool bmp_execute(char* filename_src, char* filename_dest);

#endif //IMAGE_TRANSFORMER_BMP_EXECUTOR_H
