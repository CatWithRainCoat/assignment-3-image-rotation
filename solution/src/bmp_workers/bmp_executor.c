
#include "bmp_executor.h"

bool bmp_execute(char* filename_src, char* filename_dest) {
    // init src image
    struct image image_src = {0};

    // try to open file
    FILE* file = NULL;
    bool is_open = file_open(&file, filename_src, "rb");
    if (!is_open) {
        error("Can't open src file");
        return false;
    }
    success("Src file opened successful");

    // try to read image
    bool is_read = read_image(file, &image_src);
    if (!is_read) {
        error("Can't read src image");
        return false;
    }
    success("Src image read successful");

    // try to close file
    bool is_close = file_close(file);
    if (!is_close) {
        image_free(&image_src);
        error("Can't close src file");
        return false;
    }
    success("Src file closed successful");

    // init dest image
    struct image image_dest = {0};

    // try to rotate image
    bool is_rotated = rotate(&image_src, &image_dest);
    if (!is_rotated) {
        image_free(&image_src);
        error("Can't rotate image");
        return false;
    }
    success("Image rotated successful");

    // free source image
    image_free(&image_src);

    // try to open file
    is_open = file_open(&file, filename_dest, "wb");
    if (!is_open) {
        image_free(&image_dest);
        error("Can't open dest file");
        return false;
    }
    success("Dest file opened successful");

    // try to write image
    bool is_write = write_image(file, &image_dest);
    if (!is_write) {
        image_free(&image_dest);
        error("Can't write dest image");
        return false;
    }
    success("Dest image read successful");

    // try to close file
    is_close = file_close(file);
    if (!is_close) {
        image_free(&image_dest);
        error("Can't close dest file");
        return false;
    }
    success("Dest file closed successful");

    // free dest image and return
    image_free(&image_dest);
    return true;
}
