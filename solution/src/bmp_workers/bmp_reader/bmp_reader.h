#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

#include "../../image_workers/image_worker.h"
#include "../bmp_commons.h"

#include <stdbool.h>
#include <stdio.h>

bool read_image(FILE* file, struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_READER_H
