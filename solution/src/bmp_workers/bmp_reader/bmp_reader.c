
#include "bmp_reader.h"

bool read_data(FILE* file, struct image* image) {
    // calc padding
    uint8_t padding;
    uint8_t ost = (image->width * sizeof(struct pixel)) % 4;
    bool is_zero = ost == 0;
    if (is_zero) {
        padding = 0;
    } else {
        padding = 4 - ost;
    }

    // get sizes
    uint64_t width = image->width;
    uint64_t height = image->height;
    size_t pixel_size = sizeof(struct pixel);

    for (int i = 0; i < height; i++) {
        // read line
        struct pixel* ptr = image->data + i * width;
        size_t read = fread(ptr, pixel_size, width, file);
        bool is_success = read == width;
        if (!is_success) {
            return false;
        }
        // skip padding
        int skipped = fseek(file, padding, SEEK_CUR);
        is_success = skipped == 0;
        if (!is_success) {
            return false;
        }
    }
    return true;
}

bool read_header(FILE* file, struct bmp_header* image) {
    // try to read header
    size_t read_count = fread(image, sizeof(struct bmp_header),
            1, file);
    // return status
    bool is_success = read_count == 1;
    return is_success;
}

bool read_image(FILE* file, struct image* image) {
    struct bmp_header header = {0};
    // try to read header
    bool is_read_header = read_header(file, &header);
    if (!is_read_header) {
        return false;
    }
    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    *image = image_allocate(width,height);
    // try to read data
    bool is_read_data = read_data(file, image);
    return is_read_data;
}
