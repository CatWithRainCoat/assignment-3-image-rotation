
#include "bmp_writer.h"


bool write_data(FILE* file, struct image* image) {
    // get sizes
    uint64_t width = image->width;
    uint64_t height = image->height;
    struct pixel* data = image->data;
    size_t pixel_size = sizeof(struct pixel);
    // calc padding
    uint8_t padding;
    uint8_t ost = (width * pixel_size) % 4;
    bool is_zero = ost == 0;
    if (is_zero) {
        padding = 0;
    } else {
        padding = 4 - ost;
    }

    for (int i = 0; i < height; i++) {
        // try to write line
        size_t write_count = fwrite(data + i * width, pixel_size, width, file);
        bool is_success = write_count == width;
        if (!is_success) {
            return false;
        }
        // try to skip padding
        size_t skipped = fseek(file, padding, SEEK_CUR);
        is_success = skipped == 0;
        if (!is_success) {
            return false;
        }
    }
    return true;
}

bool write_header(FILE* file, struct image* image) {
    // get sizes
    uint64_t width = image->width;
    uint64_t height = image->height;
    size_t pixel_size = sizeof(struct pixel);
    // calc padding
    uint8_t padding;
    uint8_t ost = (width * pixel_size) % 4;
    bool is_zero = ost == 0;
    if (is_zero) {
        padding = 0;
    } else {
        padding = 4 - ost;
    }
    // get sizes
    uint32_t image_size = pixel_size * (width + padding) * height;
    size_t header_size = sizeof(struct bmp_header);
    uint32_t file_size = image_size + header_size;

    // create header
    struct bmp_header header = {0};
    header.bfType = BF_TYPE;
    header.bfileSize = file_size;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = B_OFF_BITS;
    header.biSize = BI_SIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = image_size;
    header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;


    size_t write_count = fwrite(&header, header_size, 1, file);
    bool is_success = write_count == 1;
    return is_success;
}

bool write_image(FILE* file, struct image* image) {
    // try to write header
    bool is_write_header = write_header(file, image);
    if (!is_write_header) {
        return false;
    }
    // try to write data
    bool is_write_data = write_data(file, image);
    if (!is_write_data) {
        return false;
    }
    return true;
}
