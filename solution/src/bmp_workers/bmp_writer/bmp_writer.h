#ifndef IMAGE_TRANSFORMER_BMP_WRITER_H
#define IMAGE_TRANSFORMER_BMP_WRITER_H

#include "../../image_workers/image_worker.h"
#include "../bmp_commons.h"

#include <stdbool.h>
#include <stdio.h>

bool write_image(FILE* file, struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_WRITER_H
