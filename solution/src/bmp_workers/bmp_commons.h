#ifndef IMAGE_TRANSFORMER_BMP_COMMONS_H
#define IMAGE_TRANSFORMER_BMP_COMMONS_H

#include <inttypes.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum DEFAULTS {
    BF_TYPE = 19778,
    BF_RESERVED = 0,
    B_OFF_BITS = sizeof(struct bmp_header),
    BI_SIZE = 40,
    BI_X_PELS_PER_METER = 2835,
    BI_Y_PELS_PER_METER = 2835,
    BI_BIT_COUNT = 24,
    BI_PLANES = 1,
    BI_COMPRESSION = 0,
    BI_CLR_USED = 0,
    BI_CLR_IMPORTANT = 0
};

#endif //IMAGE_TRANSFORMER_BMP_COMMONS_H
