
#include "bmp_rotator.h"

bool rotate(struct image* image_src, struct image *image_dest) {
    // get sizes
    uint64_t width_src = image_src->width;
    uint64_t height_src = image_src->height;
    struct pixel* data_src = image_src->data;

    // alloc image
    *image_dest = image_allocate(height_src, width_src);
    struct pixel* data_dest = image_dest->data;

    // rotate
    for (uint64_t row = 0; row < height_src; row++) {
        for (uint64_t col = 0; col < width_src; col++) {
            // get pixel
            struct pixel* pixel = data_src + (width_src * row + col);
            // set pixel
            data_dest[height_src * col + height_src - row - 1] = *pixel;
        }
    }
    return true;
}
