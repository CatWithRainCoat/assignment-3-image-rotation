#ifndef IMAGE_TRANSFORMER_BMP_ROTATOR_H
#define IMAGE_TRANSFORMER_BMP_ROTATOR_H

#include "../../image_workers/image_worker.h"

#include <stdbool.h>

bool rotate(struct image* image_src, struct image *image_dest);

#endif //IMAGE_TRANSFORMER_BMP_ROTATOR_H
