#ifndef IMAGE_TRANSFORMER_FILE_WORKER_H
#define IMAGE_TRANSFORMER_FILE_WORKER_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE** target, char* filename, char* mode);
bool file_close(FILE* target);

#endif //IMAGE_TRANSFORMER_FILE_WORKER_H
