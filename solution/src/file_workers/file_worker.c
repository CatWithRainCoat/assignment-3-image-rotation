#include "file_worker.h"

bool file_open(FILE** target, char* filename, char* mode) {
    FILE* f;
    f = fopen(filename, mode);
    *target = f;
    bool is_null = f == NULL;
    return !is_null;
}

bool file_close(FILE* target) {
    int result = fclose(target);
    bool is_success = result == 0;
    return is_success;
}
