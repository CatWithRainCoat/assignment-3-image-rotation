#include "bmp_workers/bmp_executor.h"
#include "utils/utils.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        error("Usage: image-transformer <src> <dest>");
        return 1;
    }

    bool is_executed = bmp_execute(argv[1], argv[2]);

    if (is_executed) {
        success("BMP image rotated successful");
        return 0;
    } else {
        error("BMP image rotate failed");
        return 1;
    }
}
