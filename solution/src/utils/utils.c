
#include "utils.h"

void success(char* message) {
    // print success message in stdout
    fprintf(stdout, "%s\n", message);
}

void error(char* message) {
    // print error message in stderr
    fprintf(stderr, "%s\n", message);
}
