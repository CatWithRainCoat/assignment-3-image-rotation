#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include <stdio.h>

void success(char* message);
void error(char* message);

#endif //IMAGE_TRANSFORMER_UTILS_H
